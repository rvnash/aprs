# Description of this Library #

This library's purpose is to send APRS packets on the Particle Photon or Particle Electron microcontrollers. Information about these devices can be found here: https://www.particle.io/

The RF code is fairly Arduino generic so it could be modified to work for pretty much any microcontroller that had these attributes:

* Support of the Arduino development libraries (Wiring)
* A digital to analog pin
* Fast enough to produce a decent sin wave at 2200Hz

# APRS #

APRS is digital communications information channel for Ham radio. It stands for Automatic Packet Reporting System. The reference documentation can be found here: http://www.aprs.org/  APRS over the internet is called APRIS-IS, where as APRS over packet radio, I call APRS-RF.

# How to use the library #
See the example code and read the comments in src/APRS.h. This should guide you through using this code successfully.

The APRS system is a amateur radio system, and these messages can find their way onto transmissions over frequencies restricted to licensed radio operators. Therefore the use of this is for licensed amateur radio operators only.

Many times APRS is used to track high altitude balloons. Per FCC 22.925, using cellular radio, like the Electron on any aircraft, including weather balloons, is illegal unless the vehicle is touching the ground. You could use a cellular tracker on a weather balloon, but you would have to make sure that the radio stayed off until the payload was on the ground

In the US a popular transmitter to use with APRS is the RadioMetrix HX1. I believe the only distributer in the US is Lemos International (limosint.com). Here is the schematic used to transmit RF using that device.

![Schematic](images/RadiometrixHX1.png)

My second tracker is based on the DORJI DRA818V transmitter. I find it to be superior to the HX1 in range and reliability. This project uses this schematic.

![Schematic2](images/DRA818V.png)
