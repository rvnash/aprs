/*
 * Copyright (C) 2017 by KC3ARY Rich Nash
 *
 * Module is based on code from EA5HAV and the Trackuino project.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#pragma once

#include <stdint.h>

struct PathAddress;

/*
 * This class is not well documented. It is meant as a utility to create
 * the AX25 bit encoding of the given input.
 */

class AX25
{
public:
  AX25(uint8_t * buf, const uint16_t bufSize);
  void send_header(const struct PathAddress * const paths, const uint16_t nPaths,
      const uint16_t preambleFlags);
  void send_byte(uint8_t a_byte);
  void send_string(const char * const string);
  void send_footer(uint16_t postambleFlags);
  uint16_t getPacketSize();
  bool isMemoryExceeded();
private:
  uint16_t crc;
  uint8_t ones_in_a_row;
  uint16_t packet_size; // size in bits of packets
  uint8_t *packet;
  uint16_t maxPacketSize;  // bytes
  bool memoryExceeded;
  void send_bit(const uint8_t a_bit, const bool bitStuff);
  void send_flag();
  void update_crc(const uint8_t a_bit);
};
