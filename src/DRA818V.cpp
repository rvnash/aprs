/*
APRS
A library for sending APRS-IS position packets using
the Particle microcontroller over the internet OR RF

Copyright (C) 2018 Richard Nash
All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "DRA818V.h"
#include "Particle.h"
#include <ParticleSoftSerial.h>

DRA818V::DRA818V(int micPin, int pttPin, int pttOnState, int pttDelay,
                 int txPin, int rxPin, int pdPin, int h_lPin) :
  Transmitter(micPin, pttPin, pttOnState, pttDelay)
{
  this->pdPin = pdPin;
  this->h_lPin = h_lPin;
  this->txPin = txPin;
  this->rxPin = rxPin;
  this->serial = NULL;
  this->frequency = 0.0f;
  pinMode(pdPin,OUTPUT);
  pinMode(h_lPin,INPUT);
}

void DRA818V::init()
{
  pinMode(pdPin,OUTPUT);
  pinMode(h_lPin,INPUT);
  setEnabled(false);
  connectSerial();
  Transmitter::init();
}

// Called to make sure we can communicate
int DRA818V::connect(uint32_t millisTimeout)
{
  uint32_t start = millis();
  while (millis()-start < millisTimeout) { //
    char resp[100];
    sendCommand("AT+DMOCONNECT");
    int timeout=getResponse(resp,100,100);
    if (!timeout && strcmp(resp,"+DMOCONNECT:0") == 0) {
      return 0;
    } else {
      connectSerial(); // Try re-initializing the serial port
      if (millis()-start < millisTimeout) delay(333); // Retry only as often as 3Hz
    }
  }
  return 1;
}

void DRA818V::sendCommand(const char *cmd)
{
  serial->write(cmd);
  serial->write("\r\n");
  serial->flush();

  if (debuggingOutput >= TRANSMITTER_VERBOSE) Serial.printf("Send DRA818V: %s\n", cmd);
}

int DRA818V::getResponse(char *resp, int len, uint32_t timeoutMS)
{
  uint32_t start = millis();
  int onChar = 0;
  *resp = '\0';
  if (debuggingOutput >= TRANSMITTER_VERBOSE) Serial.printf("Got DRA818V: ");
  while (millis()-start < timeoutMS) {
    if (serial->available()) {
      char c = serial->read();
      if (c >= 32 && c <= 126) {
        if (debuggingOutput >= TRANSMITTER_VERBOSE) Serial.write(c);
      } else {
        if (debuggingOutput >= TRANSMITTER_VERBOSE) Serial.printf("x%02x",c);
      }
      if (onChar == 0) {
        if (c != '+') { // All replies start with '+'
          if (debuggingOutput >= TRANSMITTER_VERBOSE) Serial.printf("Unexpected char: %d\n", c);
          continue;
        }
      }
      if (onChar < len-1) {
        resp[onChar++] = c;
        resp[onChar] = '\0'; // Always keep it null terminated
      }
      if (onChar > 1) {
        if (resp[onChar-2] == '\r' && resp[onChar-1] == '\n') {
          resp[onChar-2] = '\0';
          if (debuggingOutput >= TRANSMITTER_VERBOSE) Serial.printf("\n");
          return 0;
        }
      }
    }
  }
  if (debuggingOutput >= TRANSMITTER_VERBOSE) Serial.printf("\n");
  return 1; // Timeout
}

int DRA818V::setTransmitParams(bool narrowBand, float freq, bool preEmphasis, bool lowPassFilter, bool highPassFilter, bool highPower)
{
  Transmitter::setFrequency(freq);
  char resp[100];
  int timeout;

  if (!this->serial) {
    Serial.printf("DRA818V NOT initialized.\n");
    return 1;
  }
  if (highPower) {
    pinMode(h_lPin,INPUT);
  } else {
    pinMode(h_lPin,OUTPUT);
    digitalWrite(h_lPin, LOW);
  }
  frequency = 0.0f;
  char command[100];
  sprintf(command,"AT+DMOSETGROUP=%d,%8.4f,%8.4f,0000,4,0000", (narrowBand ? 0 : 1), freq, freq);
  sendCommand(command);
  timeout=getResponse(resp,100,100);
  if (!timeout && strcmp(resp,"+DMOSETGROUP:0") == 0) {
    frequency = freq;
  } else {
    Serial.printf("Failed to set frequncy (%s) resp %s\n", command, resp);
    return 1;
  }
  sprintf(command,"AT+SETFILTER=%d,%d,%d", (preEmphasis ? 0 : 1), (highPassFilter ? 0 : 1), (lowPassFilter ? 0 : 1));
  sendCommand(command);
  timeout=getResponse(resp,100,100);
  if (timeout || strcmp(resp,"+DMOSETFILTER:0") != 0) {
    Serial.printf("Failed to set filters (%s) resp %s\n", command, resp);
    return 1;
  }
  return 0;
}

void DRA818V::setEnabled(bool flag)
{
  if (flag) {
    if (debuggingOutput >= TRANSMITTER_VERBOSE) Serial.printf("DRA818V ENABLED\n");
    digitalWrite(pdPin,HIGH);
  } else {
    if (debuggingOutput >= TRANSMITTER_VERBOSE) Serial.printf("DRA818V DISABLED\n");
    digitalWrite(pdPin,LOW);
    pinMode(h_lPin,INPUT);
  }
  enabled = flag;
}

void DRA818V::connectSerial()
{
  if (rxPin == RX && txPin == TX) {
    Serial1.begin(9600);
    serial = &Serial1;
    return;
  } else {
    ParticleSoftSerial *tmp;
    if (serial) {
      tmp = (ParticleSoftSerial *)serial;
      tmp->end();
    } else {
      // Serial.printf("Initialize soft with rx %d tx %d\n",rxPin,txPin);
      tmp = new ParticleSoftSerial(rxPin,txPin);
    }
    tmp->begin(9600);
    serial = tmp;
  }
}
