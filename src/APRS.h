/*
APRS
A library for sending APRS-IS position packets using
the Particle microcontroller over the internet OR RF

Copyright (C) 2018 Richard Nash
All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*
 * NOTE: APRS is an amateur radio system, and these messages can find their
 * way onto transmissions over frequencies restricted to licensed radio operators.
 * Therefore the use of this is for licensed amateur radio operators only. This
 * applies to both the internet implementation AND the radio frequency implementation.
 *
 * Do not use this library unless you are a licensed amateur radio operator.
 */

#pragma once

#include "Transmitter.h"
#include "application.h"

#define APRS_STATUS_OK 0
#define APRS_STATUS_POST_FAILED  1
#define APRS_STATUS_CONNECTION_FAILED  3
#define APRS_STATUS_ERROR  4

// Used to pass in the path array
// Always start with destination, source, then other path element
struct PathAddress
{
  const char *callsign;
  uint8_t ssid;
};

// Define this to get some debugging on Serial
#define APRS_DEBUGGING

class APRS {
public:

/*
 * APRS (Constructor)
 *
 * Parameters:
 * transmitter - The Transmitter object or subclass
 * preambleFlags - How many flags (0x7e) to transmit before the message. At least 2, typically 10 or more
 * postambleFlags - How many flags (0x7e) to transmit after the message. At least 2, typically 10 or more
 *
 */
APRS( Transmitter &transmitter, uint16_t preambleFlags, uint16_t postambleFlags  );

/*
 * IS_send - Connects and sends an APRS position packet using TCPIP
 * Parameters:
 *  paths - Structure of paths, starting DEST, SOURCE
 *  nPaths - Number of structures in the paths array (only 2 supported, DEST and SOURCE)
 *  server - The server address. Example: "rotate.aprs2.net"
 *           See http://www.aprs-is.net/Connecting.aspx for information on connecting
 *  port - The port on that server that accepts HTTP, exampleL 8080
 *  password - The computed pass code for the given sourceStation
 *  day, hour, minute - The current day of the month (0..30), hour (0..23), and minute (0..59)
 *  latitude, longitude - The position in floating point degrees
 *  altitudeFeet - In floating feet (will be rounded to nearest foot)
 *  course - In degrees (0..359)
 *  speed - In knots
 *  message - Any comment or message appended to the packet. Pass in "", not null, for no message
 *
 * Return codes:
 *    APRS_STATUS_OK - if the message was sent and acknowledged with HTTP 200
 *    APRS_STATUS_CONNECTION_FAILED - if the connection could not be made.
 *    APRS_STATUS_POST_FAILED - if the message was negatively acknowledged
 */
uint32_t IS_send(
  const PathAddress * const paths, uint32_t nPaths,
  const char* server, uint32_t port, const char *password,
  int day, int hour, int minute,
  float latitude, float longitude, float altitudeFeet,
  int courseDegrees, int speedKnots,
  char symbolTable, char symbol,
  const char *message);

  /*
   * RF_send - Creates AFSK audio of the AX25 encoded APRS packet.
   * What this means is if you use feed this audio into a transmitter which is
   * transmitting on the APRS frequency in your area (144.390 MHz for the U.S.)
   * and that audio is picked up by a digipeater or iGate, your message will be
   * join the APRS network.
   * Parameters
   *   paths - Structure of paths, starting DEST, SOURCE, intermediate paths
   *   nPaths - Number of elements in paths
   *   day, hour, minute - The current day of the month (0..30), hour (0..23), and minute (0..59)
   *   latitude, longitude - The position in floating point degrees
   *   altitudeFet - In floating feet (will be rounded to nearest foot)
   *   course - In degrees (0..359)
   *   speed - In knots
   *   message - Any comment or message appended to the packet. Pass in "", not null, for no message
   *   voltsPtP - How many volts Peek to Peek to output, 3.14V is the maximum
   *   fCutoffFreq - This is for preemphasis. Take a look at https://en.wikipedia.org/wiki/Emphasis_(telecommunications)#Pre-emphasis
   *                 If you pass in a frequency, then the code will implement high pass filter
   *                 Amplifying the 22Khz audio signal above the 12Khz signal. Whether or not this is desirable is
   *                 dependant on the reciever. If the receiver applies De-emphasis, then
   *                 this will help, otherwise it won't. Pass in 0.0 to disable preemphasis.

   * Returns
   *   APRS_STATUS_OK on success
   *   APRS_STATUS_ERROR on failure
   */
uint32_t RF_send(
    const PathAddress * const paths, uint32_t nPaths,
    int day, int hour, int minute,
    float latitude, float longitude, float altitudeFeet,
    int courseDegrees, int speekKnots,
    char symbolTable, char symbol,
    const char *messsage,
    float voltsPtP,
    float fCutoffFreq=0.0);

constexpr static float MAX_VOLTS_PTP = 3.14f;

private:
  const static int SAMPLE_POINTS = 512;  // Number of points in the sinwave table, Must be power of 2
  constexpr static float DEFAULT_FREQ = 144.39f;
  Transmitter &transmitter;
  uint16_t sinWave[SAMPLE_POINTS];
  uint16_t preambleFlags;
  uint16_t postambleFlags;
  const char* latToStr(float lat);
  const char* lonToStr(float lon);
  const char* constructContent(
      int day, int hour, int minute,
      float latitude, float longitude, float altitudeFeet,
      int courseDegrees, int speedKnots,
      char symbolTable, char symbol,
      const char *message);
  void logBuffer(const uint8_t * const buf, const int32_t bitsSent);
  uint32_t afsk_send(unsigned char *bytes, uint32_t bits, float fCutoffFreq);
  void sendTones(unsigned char *buf, uint32_t numBits, float fCutoffFreq);
  void setSinewaveTable(float voltsPtP);
};
