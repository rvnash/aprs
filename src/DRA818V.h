/*
APRS
A library for sending APRS-IS position packets using
the Particle microcontroller over the internet OR RF

Copyright (C) 2018 Richard Nash
All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include "Particle.h"
#include "Transmitter.h"

class DRA818V : public Transmitter
{
public:
  // Constructor
  // micPin - The pin that can support DAC
  // pttPin - The PTT Pin
  // pttOnState - Which state, HIGH or LOW for transmit
  // txPin - The serial port transmit pin (will use Software Serial if need be)
  // rxPin - The serial port recieve pin
  // pdPin - The pin connected to chip enable
  // h_lPin - Set HIGH for low power (.5W), LOW for HIGH power (1W) (circuit should have open pulldown transistor)
  DRA818V(int micPin, int pttPin, int pttOnState, int pttDelay,
          int txPin, int rxPin, int pdPin, int h_lPin);

  // Need to call this after construction in setup
  void init();

  // Need to call this after enablement to start communication
  // Returns 1 if it fails failed
  int connect(uint32_t millisTimeout);

  // Sets the transmitters parameters
  // See documentation for details: http://www.nicerf.com/Upload/ueditor/files/2017-05-12/SA818%20programming%20manual-87b79dc0-2955-49b4-bf35-84c59b1d6321.pdf
  // narrowBand - false = 25Khz, true= 12.5Khz
  // freq - Frequency in MHz
  // preEmphasis - true = amplify higher frequencies
  // lowPassFilter - true = apply low pass filter to audio
  // highPassFilter - true = apply high pass filter to audio
  // highPower - true = 1W, false = 0.5W
  // Returns 1 on failure
  int setTransmitParams(bool narrowBand, float freq, bool preEmphasis, bool lowPassFilter, bool highPassFilter, bool highPower);

  // Turns off chip to save power. Default is enabled
  void setEnabled(bool flag);
  bool getEnabled() { return enabled; };


private:
  Stream *serial;
  int pdPin, h_lPin;
  int txPin, rxPin;
  bool enabled;
  void sendCommand(const char *cmd);
  int getResponse(char *resp, int len, uint32_t timeoutMS);
  void connectSerial();
};
