/*
APRS
A library for sending APRS-IS position packets using
the Particle microcontroller over the internet OR RF

Copyright (C) 2018 Richard Nash
All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "Transmitter.h"

Transmitter::Transmitter(int audioPin, int pttPin, int pttOnState, uint32_t pttDelay)
{
  this->pttPin = pttPin;
  this->pttOnState = pttOnState;
  this->audioPin = audioPin;
  this->pttDelay = pttDelay;
  this->frequency = 0.0f;
  this->fPTT = false;
  this->debuggingOutput = TRANSMITTER_SILENT;
}

void Transmitter::init()
{
  // Setup PTT Pin
  if (pttPin != -1) {
    // High impedance state is RX, pttOnState is TX state, which is LOW if directly tied to transmitter
    pinMode(pttPin,INPUT);
  }
  // Setup Audio out pin
  pinMode(audioPin,OUTPUT);
  analogWrite(audioPin, 2047);
}

void Transmitter::setFrequency(float freq)
{
  frequency = freq; // Doesn't do anything with this, just remembers it.
}

void Transmitter::setPTT(bool flag)
{
  if (pttPin != -1) {
    if (flag) {
      if (debuggingOutput == TRANSMITTER_VERBOSE) Serial.printf("PTT ON\n",pttOnState);
      pinMode(pttPin,OUTPUT);
      digitalWrite(pttPin,pttOnState);
      delay(pttDelay);
    } else {
      pinMode(pttPin,INPUT);
      if (debuggingOutput == TRANSMITTER_VERBOSE) Serial.printf("PTT OFF\n");
    }
  }
  fPTT = flag;
}
