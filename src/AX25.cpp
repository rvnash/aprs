/*
 * Copyright (C) 2017 by KC3ARY Rich Nash
 *
 * Module is based on code from EA5HAV and the Trackuino project.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "application.h"
#include "AX25.h"
#include "APRS.h"

AX25::AX25(uint8_t *buf, const uint16_t bufSize)
{
  packet = buf;
  maxPacketSize = bufSize;
  memoryExceeded = false;
  packet_size = 0;
  ones_in_a_row = 0;
  crc = 0xffff;

  // Serial.println("Going to write out the packet");
}

void AX25::send_header(const struct PathAddress * const paths,
                      const uint16_t nPaths,
                      const uint16_t preambleFlags)
{
  uint16_t i, j;

  for (i = 0; i < preambleFlags; i++) {
    send_flag();
  }

  for (i = 0; i < nPaths; i++) {
    // Transmit callsign
    for (j = 0; paths[i].callsign[j]; j++)
      send_byte(paths[i].callsign[j] << 1);
    // Transmit pad
    for (; j < 6; j++)
      send_byte(' ' << 1);
    // Transmit SSID. Termination signaled with last bit = 1
    if (i == nPaths - 1)
      send_byte((('0' + paths[i].ssid) << 1) | 1);
    else
      send_byte(('0' + paths[i].ssid) << 1);
  }

  // Control field: 3 = APRS-UI frame
  send_byte(0x03);

  // Protocol ID: 0xf0 = no layer 3 data
  send_byte(0xf0);
}

void AX25::send_byte(uint8_t a_byte)
{
  // Serial.write(a_byte);

  for (uint8_t i = 0; i < 8; ++i) {
    send_bit(a_byte & 1, true);
    a_byte >>= 1;
  }
}

void AX25::send_string(const char * const string)
{
  for (uint32_t i = 0; string[i]; i++) {
    send_byte(string[i]);
  }
}

void AX25::send_footer(uint16_t postambleFlags)
{
  // Save the crc so that it can be treated it atomically
  uint16_t final_crc = ~crc;

  // TODO:
  // The spec says:
  // With the exception of the FCS field, all fields of an AX.25 frame shall be sent with each octet's least-significant bit first.
  // The FCS shall be sent most-significant bit first.
  // Why isn't this code doing that? Why is it instead inverting the FCS?

  // Send the CRC
  send_byte(final_crc & 0xff);
  final_crc >>= 8;
  send_byte(final_crc & 0xff);

  // Signal the end of frame
  for (int i = 0; i < postambleFlags; i++) {
    send_flag();
  }
}

uint16_t AX25::getPacketSize()
{
  return packet_size;
}

bool AX25::isMemoryExceeded()
{
  return memoryExceeded;
}

// Private functions
void AX25::update_crc(const uint8_t a_bit)
{
  crc ^= (uint16_t)a_bit;  // XOR lsb of CRC with the latest bit
  if (crc & (uint16_t)1) {
    crc = (crc >> 1) ^ (uint16_t)0x8408;
  } else {
    crc >>= 1;
  }
}


void AX25::send_bit(const uint8_t a_bit, const bool bitStuff)
{
  if (maxPacketSize <= (packet_size >> 3)) {
    memoryExceeded = true;
    return; // We ran out of buffer space
  }
  if (bitStuff)
    update_crc(a_bit);
  if (a_bit) {
    packet[packet_size >> 3] |= (1 << (packet_size & 7));
    packet_size++;
    if (bitStuff && ++ones_in_a_row == 5) {
      send_bit(0, false); // Stuff a 0 bit
    }
  } else {
    ones_in_a_row = 0;
    packet[packet_size >> 3] &= ~(1 << (packet_size & 7));
    packet_size++;
  }
}

void AX25::send_flag()
{
  // Serial.write(0x7e);
  // Send 0x7e without bit stuffing
  send_bit(0, false);
  send_bit(1, false);
  send_bit(1, false);
  send_bit(1, false);
  send_bit(1, false);
  send_bit(1, false);
  send_bit(1, false);
  send_bit(0, false);
}
