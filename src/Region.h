#pragma once

struct LongLat
{
public:
  float lon, lat;
};

class Region
{
public:
  const char *name;
  float frequency;
  bool allowAirborneAPRS;

  Region(const char *_name, float _frequency, LongLat _poly[], int _n, bool _allowAirborneAPRS):
        name(_name), frequency(_frequency), allowAirborneAPRS(_allowAirborneAPRS), poly(_poly), n(_n) {}
  /*
   * getLocalRegion - Returns the RF signal's frequency in Megahertz appropriate the given location.
   * Parameters
   *   longitude, latitude - The position in floating point degrees
   * Returns
   *   The Region information
   */
  static Region *getLocalRegion(float longitude, float latitude);
private:
  LongLat *poly;
  int n;

  bool pointInRegion(float longitude, float latitude);
};
