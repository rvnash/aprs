/*
APRS
A library for sending APRS-IS position packets using
the Particle microcontroller over the internet OR RF

Copyright (C) 2018 Richard Nash
All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "APRS.h"
#include "application.h"
#include <stdlib.h>
#include "AX25.h"
#include "math.h"

APRS::APRS( Transmitter &p_transmitter, uint16_t preambleFlags, uint16_t postambleFlags  ) : transmitter(p_transmitter)
{
  this->preambleFlags = preambleFlags;
  this->postambleFlags = postambleFlags;
  setSinewaveTable(3.14f);
}

uint32_t APRS::IS_send(
                        const PathAddress * const paths, uint32_t nPaths,
                        const char* server, uint32_t port, const char *password,
                        int day, int hour, int minute,
                        float latitude, float longitude, float altitudeFeet,
                        int courseDegrees, int speedKnots,
                        char symbolTable, char symbol,
                        const char *message)
{
  TCPClient client;

  // Only support dest and source for now.
  // It isn't clear to me why you would need to specify a path for APRS IS
  if (nPaths != 2) return APRS_STATUS_ERROR;

  int32_t retry = 1;
  do {
#ifdef APRS_DEBUGGING
    Serial.printf("Connecting to %s port %d try %d\n", server, port, retry);
#endif
    delay(1000);
    if (!client.connect(server, port) || !client.connected()) {
#ifdef APRS_DEBUGGING
      Serial.println("Connection failed");
#endif
      client.stop();
      delay(1000);
      retry+=1;
    }
  } while (!client.connected() && retry < 3);
  if (!client.connected()) {
    return APRS_STATUS_CONNECTION_FAILED;
  }

  // login line\nHeader followed by /
  const char* content = constructContent(
            day, hour, minute
            , latitude, longitude, altitudeFeet
            , courseDegrees, speedKnots
            , symbolTable, symbol, message);

  char login[50], aprsContent[128], httpHeader[200];
  sprintf(login,"user %s-%d pass %s\r\n", paths[1].callsign, paths[1].ssid, password);
  sprintf(aprsContent,"%s-%d>%s-%d,TCPIP*:/%s\r\n",
                          paths[1].callsign, paths[1].ssid, paths[0].callsign, paths[0].ssid,
                          content);

  // HTTP header
  sprintf(httpHeader,"POST / HTTP/1.1\n"
                                     "Host: %s\n"
                                     "Accept-Type: text/plain\n"
                                     "Content-Type: application/octet-stream\n"
                                     "Content-Length: %d\r\n\r\n",
                                     server, strlen(login) + strlen(aprsContent));
  client.flush();
  client.write((uint8_t *)httpHeader, strlen(httpHeader));
  client.write((uint8_t *)login, strlen(login));
  client.write((uint8_t *)aprsContent, strlen(aprsContent));
  client.flush();

  Serial.write((uint8_t *)httpHeader, strlen(httpHeader));
  Serial.write((uint8_t *)login, strlen(login));
  Serial.write((uint8_t *)aprsContent, strlen(aprsContent));

  // Read back response
  int32_t i=0;
  bool foundNewline = false;
  uint32_t timeout = millis();
  char response[256];
  response[0] = 0;
  while (!foundNewline && (millis() - timeout < 5000)) {
    if (client.available()) {
      char c = client.read();
#ifdef APRS_DEBUGGING
      Serial.print(c);
#endif
      if (c == '\n') foundNewline = true;
      if (i < 255) {
        response[i++] = c; // Add character to response string
        response[i] = 0;
      }
    }
    delay(1);
  }
  client.stop(); // Close the connection to server.
  if (!strstr(response, "200")) {
#ifdef APRS_DEBUGGING
    Serial.printf("Got response: %s\n", response);
#endif
    return APRS_STATUS_POST_FAILED;
  }
  return APRS_STATUS_OK;
}

uint32_t APRS::RF_send(
    const PathAddress * const paths, uint32_t nPaths,
    int day, int hour, int minute,
    float latitude, float longitude, float altitudeFeet,
    int courseDegrees, int speedKnots,
    char symbolTable, char symbol,
    const char *message,
    float voltsPtP,
    float fCutoffFreq)
{
  uint8_t buf[512];  // 512 should be plenty.
  setSinewaveTable(voltsPtP);
  AX25 ax25(buf,sizeof(buf));
  const char * content = constructContent(
            day, hour, minute
            , latitude, longitude, altitudeFeet
            , courseDegrees, speedKnots
            , symbolTable, symbol, message);
  ax25.send_header(paths, nPaths, preambleFlags);
  ax25.send_byte('/'); // Report w/ timestamp, no APRS messaging.
  ax25.send_string(content);
  ax25.send_footer(postambleFlags);
  if (ax25.isMemoryExceeded()) return APRS_STATUS_ERROR;
  sendTones(buf, ax25.getPacketSize(),fCutoffFreq);
  logBuffer(buf, ax25.getPacketSize());
  return APRS_STATUS_OK;
}

// Private

const char* APRS::latToStr(float lat)
{
  static char returnStr[9]; // 8 characters + null eg. "4032.53N" "07957.36W"
  int32_t deg, min, minTenths;
  char northSouth = 'N';

  if (lat < 0) {
    lat = -lat;
    northSouth = 'S';  // Southern hemisphere
  }
  deg = (int32_t)lat;
  lat = (lat - (float)deg) * 60.0f;
  min = (int32_t)lat;
  lat = (lat - (float)min) * 100.0f;
  minTenths = (int32_t)lat;
  sprintf(returnStr, "%02ld%02ld.%02ld%c", deg, min, minTenths, northSouth);
  return returnStr;
}

const char* APRS::lonToStr(float lon)
{
  static char returnStr[10]; // 9 characters + null eg. "07957.36W"
  int32_t deg, min, minTenths;
  char eastWest = 'E';

  if (lon < 0) {
    lon = -lon; // Western hemisphere
    eastWest = 'W';
  }
  deg = (int32_t)lon;
  lon = (lon - (float)deg) * 60.0f;
  min = (int32_t)lon;
  lon = (lon - (float)min) * 100.0f;
  minTenths = (int32_t)lon; // Round the tenths
  sprintf(returnStr, "%03ld%02ld.%02ld%c", deg, min, minTenths, eastWest);
  return returnStr;
}

const char* APRS::constructContent(
    int day, int hour, int minute,
    float latitude, float longitude, float altitudeFeet,
    int courseDegrees, int speedKnots,
    char symbolTable, char symbol,
    const char *message)
{
  static char returnStr[84];   // Allow for a short message, 43 + message of 40 + null so eg. "071935z4032.53N/07957.36WO165/000/A=001059/<MESSAGE>"
  sprintf(returnStr, "%02d%02d%02dz%s%c%s%c%03d/%03d/A=%06d/%.40s",
                        day, hour, minute,
                        latToStr(latitude), symbolTable, lonToStr(longitude), symbol,
                        courseDegrees, speedKnots,
                        (int)(altitudeFeet+0.5f),
                        message);
  return returnStr;
}

// Print out the packet in a semi-readable way
// Note, the end of header and CRC are sent as ASCII characters, which they aren't
void APRS::logBuffer(const uint8_t * const buf, const int32_t bitsSent)
{
  if (transmitter.getDebuggingOutput() < TRANSMITTER_NORMAL) return;
  Serial.printf("Bits in packet %d: ", bitsSent);
  uint8_t frameState = 0; // 0-No start, 1-start, 2-in header, 3-In info
  uint8_t bSoFar = 0x00;
  uint8_t gotBit = 0;
  int32_t numOnes = 0;
  for (int32_t onBit = 0; onBit < bitsSent; onBit++) {
    uint8_t bit = buf[onBit >> 3] & (1 << (onBit & 7));
    if (numOnes == 5) {
      // This may be a 0 due to bit stuffing
      if (bit) { // Maybe it's a 0x7e
        onBit++;
        bit = buf[onBit >> 3] & (1 << (onBit & 7));
        if (gotBit == 6 && bSoFar == 0x3e && !bit) {
          // Got 0x7e frame start/end
          if (frameState == 0) {
            frameState = 1;
            Serial.print('[');
          } else
            if (frameState == 3 || frameState == 2) {
              frameState = 0;
              Serial.print(']');
            }
          bSoFar = 0x00;
          gotBit = 0;
        } else {
          Serial.print('X'); // Error
        }
      }
      numOnes = 0;
      continue;
    }
    if (bit) {
      // Set the one bit
      bSoFar |= (1 << gotBit);
      numOnes++;
    } else {
      numOnes = 0;
    }
    gotBit++;
    if (gotBit == 8) {
      // Got a byte;
      if (frameState == 1) {
        frameState = 2;
      } else {
        if (frameState == 2 && bSoFar == 0xf0) { // 0xf0 is the last byte of the header
          frameState = 3;
        }
      }
      char b;
      if (frameState == 2) {
        // In header
      	b = (bSoFar >> 1);
      } else {
        // In info
	      b = bSoFar;
      }
      Serial.print((b >= 32 && b <=126) ? b : '#');
      bSoFar = 0x00;
      gotBit = 0;
    }
  }
  Serial.println();
}

// This code sends the bits using AFSK. The general idea is to loop through time
// counting microseconds as fast as possible, for each instant in time the phase of
// the sin wave is calculated, and output over the DAC. Once the period for a bit is up,
// at 1200 baud, move on to the next bit. Switching between 1200 and 2200Hz for each 0 bit.
#define SAMPLE_POINTS_MASK      (SAMPLE_POINTS-1)
#define SAMPLES_FIXED_POINT     2048
#define SAMPLES_FIXED_POINT_BITS 11
#define SAMPLES_PER_USEC_1200   (uint32_t)((SAMPLES_FIXED_POINT * 1200.0f * SAMPLE_POINTS) / 1000000.0f)  // MARK
#define SAMPLES_PER_USEC_2200   (uint32_t)((SAMPLES_FIXED_POINT * 2200.0f * SAMPLE_POINTS) / 1000000.0f)  // SPACE
#define REAL_USEC_BIT           (uint32_t)(1000000.0f / (1200.0f))  // BAUD rate
#define MIN_DEFLECTION          (uint16_t)100    // Don't use the whole DAC because of clipping issues at the ends
#define SCALE_DEFLECTION        (uint16_t)(4096-MIN_DEFLECTION*2)

#pragma GCC push_options
#pragma GCC optimize ("O3")
// Send the Frequency Shift Key'ed audio out the DAC pin
uint32_t APRS::afsk_send(unsigned char *bytes, uint32_t bits, float fCutoffFreq)
{
  uint32_t samplesPerUSecond = SAMPLES_PER_USEC_2200; // Start w/ Hi Frequency
  uint32_t onBit = 0;
  uint32_t cycleCount = 0;
  uint32_t startSample = 0;
  uint32_t sample=startSample;
  uint32_t startPhaseUSecond = micros();
  uint32_t lastBit = startPhaseUSecond;
  int16_t dacOut;

  // Needed for preemphasis
  float fFiltered = 0;
  float fPostFilterScale = 1.0;
  float fMaxFiltered = 0.0;
  int16_t lastDacOut=2047;
  uint32_t lastSample = startPhaseUSecond;
  float RC = (1000000.0f / (2.0f * M_PI * fCutoffFreq)); // Time constant in micros
  bool preemphasis = (fCutoffFreq > 0.0);

  while (onBit < bits) {
    uint32_t now = micros();
    sample = startSample + (now - startPhaseUSecond) * samplesPerUSecond;
    dacOut = sinWave[(sample >> SAMPLES_FIXED_POINT_BITS) & SAMPLE_POINTS_MASK];
    if (preemphasis) {
      // Implement a High Pass emphaiss
      // Essentially it is:
      // alpha = (RC / (RC + deltaT))
      // out[i] = alpha * (out[i-1] + in[i] - in[i-1])
      // See: https://en.wikipedia.org/wiki/High-pass_filter
      float fAlpha = RC / (RC + (float)(now - lastSample));
      fFiltered = fAlpha * (fFiltered + (float)(dacOut - lastDacOut));
      lastDacOut = dacOut;
      lastSample = now;
      if (fabs(fFiltered) > fMaxFiltered) { // Dynamically scale the plan to full deflection based on max value seen.
        fMaxFiltered = fabs(fFiltered);
        fPostFilterScale = SCALE_DEFLECTION / (2*fMaxFiltered);
      }
      // Since the filter takes out DC entirely, need to restore to 0..4096
      dacOut = (fPostFilterScale * fFiltered) + 2047.5f;
    }
    transmitter.setAudioSample(dacOut);
    if (now - lastBit >= REAL_USEC_BIT) { // If we've sent one bits worth of tone, go to the next bit
      onBit++;
      lastBit += REAL_USEC_BIT;
      if (((bytes[onBit >> 3] >> (onBit & 0x7)) & (uint8_t)1) == 0) { // If the next bit is a 0, swap frequencies
        // Swap frequencies
        samplesPerUSecond ^= (SAMPLES_PER_USEC_2200 ^ SAMPLES_PER_USEC_1200);
        startPhaseUSecond = now;
        startSample = sample;
      }
    }
    cycleCount++;
  }
  return cycleCount;
}
#pragma GCC pop_options

void APRS::sendTones(unsigned char *buf, uint32_t numBits, float fCutoffFreq)
{
  transmitter.setPTT(true);
  uint32_t start, cycleCount, end;
  ATOMIC_BLOCK() { // Cannot block for any reason in this loop.
    start = micros();
    cycleCount = afsk_send(buf, numBits, fCutoffFreq);
    end = micros(); // Record finish time
  }
  transmitter.setPTT(false);
  // transmitter.setAudioSample(2047);
  float samplesPerSecond = (1000000.0f * cycleCount) / (float)(end - start);
  if (transmitter.getDebuggingOutput() >= TRANSMITTER_NORMAL)
    Serial.printf("Took %5.3f secs to send, %d samples sent, samples per second %f\n", (float)(end - start)/1000000.0f, cycleCount, samplesPerSecond);
}

void APRS::setSinewaveTable(float voltsPtP)
{
  // Peek-to-peek is 3.14 full scale
  float voltsScale = voltsPtP / MAX_VOLTS_PTP;
  for (uint32_t i = 0; i < SAMPLE_POINTS; i++) {
    uint16_t sample = (uint16_t)(((float)(SCALE_DEFLECTION/2) * (1.0f + sin((2.0f * M_PI / SAMPLE_POINTS) * i))) * voltsScale + 0.5f) + MIN_DEFLECTION;
    sinWave[i] = sample;
  }
}
