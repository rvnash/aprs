/*
APRS
A library for sending APRS-IS position packets using
the Particle microcontroller over the internet OR RF

Copyright (C) 2018 Richard Nash
All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include "Particle.h"

#define TRANSMITTER_SILENT 0
#define TRANSMITTER_NORMAL 1
#define TRANSMITTER_VERBOSE 2

class Transmitter
{
public:
  /*
   * The RF call RF_send can operate in two modes, Push-To-Talk (PTT)
   * or Voice Operated Exchange (VOX). pttPin being -1 is how to indicate VOX mode.
   * In VOX mode send a toneLength. The audible tone will be sent for that
   * many milliseconds to open up VOX, then the packet will begin.
   * In PTT mode the pin given will be raised high, and then pttDelay ms later, the packet will
   * begin.
   */
  Transmitter(int audioPin, int pttPin, int pttOnState, uint32_t pttDelay );
  void init();
  void setDebuggingOutput(int f) { debuggingOutput = f;};
  int getDebuggingOutput() { return debuggingOutput;};

  virtual void setFrequency(float freq);
  virtual void setPTT(bool flag);
  inline void setAudioSample(int16_t sample) { analogWrite(audioPin, sample); }
  virtual float getFrequency() { return frequency; };
  virtual bool getPTT() { return fPTT; };

protected:
  int pttPin, pttOnState, audioPin;
  uint32_t pttDelay;
  float frequency;
  bool fPTT;
  int debuggingOutput;
};
