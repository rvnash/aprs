#include "Region.h"
#include <stddef.h>

bool Region::pointInRegion(float longitude, float latitude)
{
  int i, j;
  bool c = false;
  for (i = 0, j = n-1; i < n; j = i++) {
    if ( ((poly[i].lat > latitude) != (poly[j].lat > latitude)) &&
          (longitude < (poly[j].lon-poly[i].lon) * (latitude-poly[i].lat) / (poly[j].lat-poly[i].lat) + poly[i].lon) )
      c = !c;
  }
  return c;
}

// Generic Regional frequencies
static Region REGION_GenericEurope("Generic Europe", 144.80f, NULL, 0, true);
static Region REGION_GenericAfrica("Generic Africa", 144.80f, NULL, 0, true);
static Region REGION_GenericNorthAmerica("Generic North America", 144.39f, NULL, 0, true);
static Region REGION_GenericSouthAmerica("Generic South America", 144.39f, NULL, 0, true);
static Region REGION_GenericNorthAsia("Generic North Asia", 144.80f, NULL, 0, true);
static Region REGION_GenericSouthAsia("Generic South Asia", 144.39f, NULL, 0, true);
static Region REGION_GenericUnknown("Generic Unknown", 144.39f, NULL, 0, true);

// Specific country frequencies
static LongLat poly_argParaUrag[] = {
  {-57.79910f, -18.67750f},
  {-53.48140f, -26.66710f},
  {-57.13990f, -29.77390f},
  {-49.62520f, -34.51560f},
  {-60.24900f, -58.56250f},
  {-73.49850f, -50.35950f},
  {-67.54390f, -21.43260f}
};
static Region REGION_ArgParaUrag("Aregentina, Paraguay, and Uruguay", 144.93f, poly_argParaUrag, sizeof(poly_argParaUrag)/sizeof(LongLat), true);

static LongLat poly_Australia[] = {
  {147.56840f, -46.92030f},
	{166.02540f, -29.15220f},
	{144.14060f, -9.18890f},
	{98.78910f, -11.69530f},
	{112.41210f, -39.77480f}
};
static Region REGION_Australia("Australia", 145.175f, poly_Australia, sizeof(poly_Australia)/sizeof(LongLat), true);

static LongLat poly_Brazil[] = {
  {-57.04100, -29.76440},
	{-49.65820, -34.45220},
	{-28.30080, -5.00000},
	{-51.50390, 4.30260},
	{-60.55660, 5.00340},
	{-74.17970, -6.23000},
	{-57.74410, -18.56290},
	{-53.34960, -26.66710}
};
static Region REGION_Brazil("Brazil", 145.575f, poly_Brazil, sizeof(poly_Brazil)/sizeof(LongLat), true);

static LongLat poly_China[] = {
  {87.18750, 49.38240},
	{77.51104, 44.59703},
	{71.63090, 36.94990},
	{93.03226, 25.16517},
	{110.39060, 15.96130},
	{124.76074, 18.47961},
	{124.54103, 36.73889},
	{132.17135, 41.06670},
	{136.35137, 47.03273},
	{123.44237, 54.41894},
	{104.58986, 45.05798}
};
static Region REGION_China("China", 144.64f, poly_China, sizeof(poly_China)/sizeof(LongLat), true);

static LongLat poly_Nicaragua[] = {
  {-88.96950, 12.3000},
	{-87.02000, 11.0400},
	{-82.91000, 11.0400},
	{-82.70510, 15.3901}
};
static Region REGION_Nicaragua("Nicaragua", 144.80f, poly_Nicaragua, sizeof(poly_Nicaragua)/sizeof(LongLat), true);

static LongLat poly_CostaRicaPanama[] = {
  {-82.91000, 11.0400},
  {-87.02000, 11.0400},
	{-80.95000, 3.94000},
	{-76.20000, 9.64000}
};
static Region REGION_CostaRicaPanama("Costa Rica and Panama", 145.01f, poly_CostaRicaPanama, sizeof(poly_CostaRicaPanama)/sizeof(LongLat), true);

static LongLat poly_Japan[] = {
  {138.71337, 49.79545},
	{130.96153, 38.83591},
	{124.66191, 34.18805},
	{124.76075, 24.68693},
	{144.22850, 23.40280},
	{156.09372, 49.72447}
};
static Region REGION_Japan("Japan", 144.66f, poly_Japan, sizeof(poly_Japan)/sizeof(LongLat), true);

static LongLat poly_NewZealand[] = {
  {179.99990, -37.78810},
	{179.99990, -55.22580},
	{154.33590, -45.82880},
	{172.26560, -28.76770}
};
static Region REGION_NewZealand("New Zealand", 144.575f, poly_NewZealand, sizeof(poly_NewZealand)/sizeof(LongLat), true);

static LongLat poly_SouthKorea[] = {
  {132.17653, 41.01310},
	{124.61794, 36.70367},
	{124.65089, 34.26179},
	{130.87903, 38.87847}
};
static Region REGION_SouthKorea("South Korea", 144.62f, poly_SouthKorea, sizeof(poly_SouthKorea)/sizeof(LongLat), true);

static LongLat poly_Thailand[] = {
  {110.23679, 15.96138},
	{93.60347, 24.72692},
	{94.89994, 6.03131},
	{104.89745, 6.07500}
};
static Region REGION_Thailand("Thailand", 145.525f, poly_Thailand, sizeof(poly_Thailand)/sizeof(LongLat), true);

static LongLat poly_Venezuela[] = {
  {-66.65410, 0.18680},
	{-60.99610, 5.41910},
	{-59.52390, 9.38400},
	{-72.15820, 12.49020},
	{-72.48780, 7.10090},
	{-67.96140, 5.72530}
};
static Region REGION_Venezuela("Venezuela", 145.01f, poly_Venezuela, sizeof(poly_Venezuela)/sizeof(LongLat), true);

// No Airborne APRS permitted in these countries
static LongLat poly_Latvia[] = {
  {26.64180, 55.68380},
	{28.17990, 56.20670},
	{27.78440, 57.33250},
	{25.00490, 58.00230},
	{24.14790, 57.17200},
	{21.78590, 57.68650},
	{20.81910, 56.07200},
	{22.19240, 56.44430},
	{25.68600, 56.18230}
};
static Region REGION_Latvia("Latvia", 144.8f, poly_Latvia, sizeof(poly_Latvia)/sizeof(LongLat), false);

static LongLat poly_NorthKorea[] = {
  {130.14189, 43.21627},
	{124.03574, 39.8949},
	{125.18292, 37.39202},
	{128.47240, 38.66888},
	{130.69478, 42.3274}
};
static Region REGION_NorthKorea("North Korea", 144.62f, poly_NorthKorea, sizeof(poly_NorthKorea)/sizeof(LongLat), false);

static LongLat poly_UK[] = {
  {-0.65920, 60.97310},
	{-7.58060, 58.07790},
	{-8.21780, 54.23960},
	{-4.76810, 53.80070},
	{-5.86670, 49.76710},
	{1.30740, 50.85450},
	{1.86770, 52.78950},
	{-2.04350, 55.97380}
};
static Region REGION_UK("United Kingdom", 144.8f, poly_UK, sizeof(poly_UK)/sizeof(LongLat), false);

static LongLat poly_Yemen[] = {
  {52.20302, 19.48287},
	{41.92009, 17.42198},
	{43.59620, 12.27820},
	{53.68201, 15.80024}
};
static Region REGION_Yemen("Yemen", 144.8f, poly_Yemen, sizeof(poly_Yemen)/sizeof(LongLat), false);


Region *Region::getLocalRegion(float longitude, float latitude)
{
	// Europe region
	if (longitude > -38.0 && longitude < 73.0) {
		if (latitude > 35.0) {
      // Northern region
      if (REGION_UK.pointInRegion(longitude,latitude)) return &REGION_UK;
      if (REGION_Latvia.pointInRegion(longitude,latitude)) return &REGION_Latvia;
      return &REGION_GenericEurope;
		} else {
      // Southern region
      if (REGION_Yemen.pointInRegion(longitude,latitude)) return &REGION_Yemen;
      if (REGION_Brazil.pointInRegion(longitude,latitude)) return &REGION_Brazil;
      return &REGION_GenericAfrica;
		}
	} else if (longitude <= -38.0) {
    // The Americas
		if (latitude > 15.5) {
      // North and Central America
      return &REGION_GenericNorthAmerica;
		} else {
      // South America
      if (REGION_ArgParaUrag.pointInRegion(longitude,latitude)) return &REGION_ArgParaUrag;
      if (REGION_Brazil.pointInRegion(longitude,latitude)) return &REGION_Brazil;
      if (REGION_Venezuela.pointInRegion(longitude,latitude)) return &REGION_Venezuela;
      if (REGION_Nicaragua.pointInRegion(longitude,latitude)) return &REGION_Nicaragua;
      if (REGION_CostaRicaPanama.pointInRegion(longitude,latitude)) return &REGION_CostaRicaPanama;
      return &REGION_GenericSouthAmerica;
		}
	} else if (longitude >= 73.0) {
    // Asia
		if (latitude > 19.2) {
      // Norther parts of Asia (north of Thailand)
      if (REGION_NorthKorea.pointInRegion(longitude,latitude)) return &REGION_NorthKorea;
      if (REGION_China.pointInRegion(longitude,latitude)) return &REGION_China;
      if (REGION_Japan.pointInRegion(longitude,latitude)) return &REGION_Japan;
      if (REGION_SouthKorea.pointInRegion(longitude,latitude)) return &REGION_SouthKorea;
      if (REGION_Thailand.pointInRegion(longitude,latitude)) return &REGION_Thailand;
      return &REGION_GenericNorthAsia;
		} else {
      // Southern parts of Asia (south of Thailand)
      if (REGION_Australia.pointInRegion(longitude,latitude)) return &REGION_Australia;
      if (REGION_NewZealand.pointInRegion(longitude,latitude)) return &REGION_NewZealand;
      if (REGION_Thailand.pointInRegion(longitude,latitude)) return &REGION_Thailand;
      return &REGION_GenericSouthAsia;
		}
	} else {
    return &REGION_GenericUnknown;
	}
}
