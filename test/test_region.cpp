#include "../src/Region.h"
#include <stdio.h>

void testRegion(float latitude, float longitude, const char *expected, float expectedFrequency)
{
  Region *r;
  r = Region::getLocalRegion(longitude,latitude);
  printf("%7.4f,%8.4f - Expect/Got: %s/%s (%7.3f/%7.3f) Allow airborne xmit: %s\n", latitude, longitude, expected, r->name, expectedFrequency, r->frequency, r->allowAirborneAPRS ? "YES" : "NO");
}

void tests()
{
  printf("Testing Region Detection\n");
  testRegion(34.0f,-96.0f,"North America",144.39f);
  testRegion(13.0f,-85.0f,"Nicaragua",144.8f);
  testRegion(6.10f,-65.23f,"Venezuela",145.01f);
  testRegion(9.0f,-82.23f,"Panama",145.01f);
  testRegion(-7.26f,-35.26f,"Brazil",145.575f);
  testRegion(-6.56f,-64.30f,"Brazil",145.575f);
  testRegion(-35.0f,-65.45f,"Aregentina",144.93f);
  testRegion(50.0f,10.0f,"Europe",144.8f);
  testRegion(52.0f,0.0f,"UK",144.8f);
  testRegion(57.0f,25.45f,"Latvia",144.8f);
  testRegion(20.0f,20.0f,"Africa",144.8f);
  testRegion(16.0f,50.0f,"Yemen",144.8f);
  testRegion(30.0f,110.0f,"China",144.64f);
  testRegion(60.0f,100.0f,"Asia",144.8f);
  testRegion(16.0f,101.0f,"Thailand",145.525f);
  testRegion(-1.0f,113.0f,"South Asia",144.39f);
  testRegion(-20.0f,130.0f,"Australia",145.175f);
  testRegion(-43.0f,172.0f,"New Zealand",144.575f);
  testRegion(36.0f,138.0f,"Japan",144.66f);
  testRegion(39.5f,127.0f,"North Korea",144.62f);
  testRegion(37.50f,127.0f,"South Korea",144.62f);
  testRegion(0.0f,-30.0f,"Mid Atlantic",144.8f);
  testRegion(0.0f,-179.0f,"Mid Pacific",144.39f);

}

int main()
{
  tests();
  return 0;
}
