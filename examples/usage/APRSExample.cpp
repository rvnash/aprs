/*
  usage.ino - Example usage for APRS library by Richard Nash.

Copyright (C) 2017 Richard Nash
All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "Particle.h"
SYSTEM_MODE(AUTOMATIC);

#include "APRS.h"
#include "HX1.h"
#include "DRA818V.h"

//#define TEST_TRANS2 // Based on the Electron with HX1
#define TEST_TRANS3 // Photon PCB with DRA818V

#ifdef TEST_TRANS2
  // APRS RF configuration
  #define APRS_DAC_PIN DAC1
  #if ( PLATFORM_ID == PLATFORM_ELECTRON_PRODUCTION )
  #define APRS_PTT_PIN C0
  #elif ( PLATFORM_ID == PLATFORM_PHOTON_PRODUCTION )
  #define APRS_PTT_PIN D0
  #endif
#endif
#ifdef TEST_TRANS3
  // APRS RF configuration
  #define APRS_DAC_PIN DAC1
  #define APRS_PTT_PIN D7
  #define APRS_ENABLE_PIN D6
  #define APRS_POWERSELECT_PIN D3
  #define TX_PIN D5
  #define RX_PIN D4
#endif

#define APRS_PREAMBLE_FLAG_COUNTS 10
#define APRS_POSTAMBLE_FLAG_COUNTS 10

// This APRS IS server seems to work for me. There are others.
// To request a passcode you may use: https://aprsdroid.org/passcode/
#define APRS_IS_SERVER  "rotate.aprs2.net"
#define APRS_IS_PORT  8080
#define APRS_IS_PASSWORD "NNNNN"

// Destination callsign: APRS (with SSID=0) is okay.
#define APRS_D_CALLSIGN      "APRS"
#define APRS_D_CALLSIGN_ID   1

// Source callsign: Please put your callsign in here
#define APRS_RF_STATION "XXXXXX"
#define APRS_RF_STATION_ID 1

// See this webpage for the list of symbols: http://www.aprs.net/vm/DOS/SYMBOLS.HTM
#define SYMBOL_TABLE '/'  // Primary symbol table
#define SYMBOL_ICON 'v'   // Van.

#define MESSAGE "Test packet"

#define TRANSMIT_PERIOD ((uint32_t)(3 * 60 * 1000))

// Initialize objects from the lib
#ifdef TEST_TRANS2
  HX1 transmitter(APRS_DAC_PIN, APRS_PTT_PIN, LOW, 60);
#endif
#ifdef TEST_TRANS3
  DRA818V transmitter(APRS_DAC_PIN, APRS_PTT_PIN, LOW, 600, TX_PIN, RX_PIN, APRS_ENABLE_PIN, APRS_POWERSELECT_PIN);
#endif
APRS aprs(transmitter, APRS_PREAMBLE_FLAG_COUNTS, APRS_POSTAMBLE_FLAG_COUNTS);

float latitude, longitude, altitudeFeet;
int courseDegrees, speedKnots;

uint32_t lastIS_send;
uint32_t lastRF_send;

void button_handler(system_event_t event, int duration, void* ){if (duration > 0) System.dfu();}

void setup()
{
  Serial.begin(9600);
  System.on(button_status, button_handler);
  // You should get a location from your GPS unit. Look at the TinyGPS library
  // 40.5673385,-79.9180295 (In the middle of one of my favorite parks)
  latitude = 40.5673385;
  longitude = -79.9180295;
  altitudeFeet = 1000;
  courseDegrees = 0;
  speedKnots = 0;

  lastIS_send = millis() - TRANSMIT_PERIOD + (uint32_t)(3 * 60 * 1000); // Allow 2 minutes
  lastRF_send = millis() - TRANSMIT_PERIOD; // Transmit right away

  delay(1000);


  transmitter.init();
  transmitter.setEnabled(true);
  int failure = transmitter.connect(2000);
  if (failure) {
    Serial.printf("*** DRA818V FAILED TO INITIALIZE ***\n");
  } else {
    Serial.printf("DRA818V ready for transmitting on %8.5f\n", transmitter.getFrequency());
  }
  failure = transmitter.setTransmitParams(false, 144.390, false, false, false, true);
  Particle.syncTime();
  // Wait until Photon receives time from Particle Cloud (or connection to Particle Cloud is lost)
  waitUntil(Particle.syncTimeDone);

}

void APRSRF()
{
  Serial.printf("APRS RF send starting\n");
  PathAddress paths[] = {
    {(char *)APRS_D_CALLSIGN, APRS_D_CALLSIGN_ID},  // Destination callsign
    {(char *)APRS_RF_STATION, APRS_RF_STATION_ID},  // Source callsign
    {"WIDE1", 1}, // Digi1 (first digi in the chain)
    {"WIDE2", 1}  // Digi2 (second digi in the chain)
  };
  int day, hour, minute;
  day = Time.day();
  hour = Time.hour();
  minute = Time.minute();
  uint32_t result = aprs.RF_send(
               paths, 4,
               day, hour, minute,
               latitude, longitude, altitudeFeet,
               courseDegrees, speedKnots,
               SYMBOL_TABLE,
               SYMBOL_ICON,
               MESSAGE, APRS::MAX_VOLTS_PTP, 0.0);
  if (result == APRS_STATUS_OK) {
    Serial.printf("APRS RF send successful\n");
  } else {
    Serial.printf("Failed APRS RF, error code %d\n", result);
  }
  lastRF_send = millis();
}

void loop()
{
  // Send every 3 minutes to not spam the APRS network
  if (millis() - lastRF_send > TRANSMIT_PERIOD) {
    APRSRF();
  }
#if ( PLATFORM_ID == PLATFORM_ELECTRON_PRODUCTION )
  if (millis() - lastIS_send > TRANSMIT_PERIOD) {
    APRSIS();
  }
#endif
}
